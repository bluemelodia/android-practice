package com.example.bluemelodia.numbershapes;
import android.util.Log;

/**
 * Created by bluemelodia on 1/3/16.
 */
public class Triangular {
    Integer number;

    public Triangular(Integer num) {
        this.number = num;
    }

    public boolean checkTriangular() {
        if (this.number != null) {
            Integer cur = 0;
            for (int n = 1; cur < this.number; n++) {
                cur = (n*(n+1))/2;
            }
            if (cur.equals(this.number)) {
                return true;
            }
            return false;
        }
        return false;
    }
}
