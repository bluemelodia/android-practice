package com.example.bluemelodia.numbershapes;
import android.util.Log;

/**
 * Created by bluemelodia on 1/3/16.
 */
public class Square {
    Integer number;

    public Square(Integer num) {
        this.number = num;
    }

    public boolean checkSquare() {
        if (this.number != null) {
            for (int n = 0; n <= Math.sqrt(this.number); n++) {
                Integer curPower = (int)Math.pow(n, 2);
                if (this.number.equals(curPower)) {
                    return true;
                }
            }
        }
        return false;
    }
}
