package com.example.bluemelodia.myfirstandroidapp;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

// Code that controls what the app does
public class MainActivity extends AppCompatActivity {

    /* Executes when the button is tapped.
     * Called by the button, which is of type View; hence the function
     * will receive a variable of type View */
    public void clickFunction(View view) {
        /* Declare var for the text field, allowing us to access its value
         View is anything on the screen - using findViewById, we search for a
         particular view by its id, look into resources (R) for our text field */
        // Must cast the view to EditText to avoid type errors
        EditText myTextField = (EditText) findViewById(R.id.textField);

        // Toast: message to user
        /* 1) Context of toast: class that contains info about the app running, sometimes
         used as a link to access Android file system and other resources - here we use the
         application context (usually will use this)
           2) Contents of message we want to display
           3) How long we want message to display for
            LENGTH_LONG: 4-5 seconds, LENGTH_SHORT: 2-3 seconds
           4) Use the show() method as an instruction show the message to the user
         */
        Toast.makeText(getApplicationContext(), "Hi Sally!", Toast.LENGTH_LONG).show();

        // Info log, gets the variable myTextField, get the text of the text field,
        // then cast to string so we can send it to the logs
        Log.i("TextField Value: ", myTextField.getText().toString());
    }

    public void clickMe(View view) {
        ImageView flowerView = (ImageView) findViewById(R.id.flowerView);
        // change the contents of the imageView on button click
        flowerView.setImageResource(R.drawable.lotus3);
        Log.i("Info", "Hi I am Sally!");
    }

    /* This code will run when the app itself runs. A lot of the code we run will be here, and it
     will run once the app finished setup and is ready to do something for the user */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
