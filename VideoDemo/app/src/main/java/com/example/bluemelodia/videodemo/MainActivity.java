package com.example.bluemelodia.videodemo;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.VideoView;
import android.widget.MediaController;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // find our video view
        VideoView videoView = (VideoView) findViewById(R.id.videoView);
        // using getPackageName(), if you changed the package later on, it will update automatically
        // getPackageName(String packageName) - attempts to locate the requested package in the caller's class loader
        videoView.setVideoPath("android.resource://" + getPackageName() + "/" + R.raw.turtle);

        // can also do online videos that get streamed to our app - but lots of issues in the emulator
        // just add the full path, http://link...run on a real device

        // need session - combo of context + session id; this gives us the session needed to define our media controller
        MediaController mediaController = new MediaController(this);

        // videoView becomes anchor view for media controller
        mediaController.setAnchorView(videoView);

        // synced things up both ways - controller for videoView is the media controller
        videoView.setMediaController(mediaController);

        // this will play the video w/o controls
        videoView.start();

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
