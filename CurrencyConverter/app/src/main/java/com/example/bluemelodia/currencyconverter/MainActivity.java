package com.example.bluemelodia.currencyconverter;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.util.Log;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    public void convertToUSD(View view) {
        EditText NTD = (EditText) findViewById(R.id.NTD);
        Double TaiwanDollar = Double.parseDouble(NTD.getText().toString());
        Double USDollar = TaiwanDollar*0.030;
        Toast.makeText(getApplicationContext(), "$" + USDollar.toString(), Toast.LENGTH_LONG).show();
        //Log.i("Info", "Convert to USD");
    }

    public void convertToNTD(View view) {
        EditText USD = (EditText) findViewById(R.id.USD);
        Double USADollar = Double.parseDouble(USD.getText().toString());
        Double TaiwanDollar = USADollar*32.92;
        Toast.makeText(getApplicationContext(), TaiwanDollar.toString() + "NT", Toast.LENGTH_LONG).show();
        //Log.i("Info", "Convert to NTD");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
