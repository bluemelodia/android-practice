package com.example.bluemelodia.higherorlower;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

import java.util.Random;

public class MainActivity extends AppCompatActivity {
    private int number;

    public void restart(View view) {
        Random randomGenerator = new Random();
        number = randomGenerator.nextInt(100)+1;
        Toast.makeText(getApplicationContext(), "Picked new number.", Toast.LENGTH_LONG).show();
        Log.i("New Number", String.valueOf(number));
    }

    public void guess(View view) {
        EditText userGuessField = (EditText) findViewById(R.id.userGuess);
        try {
            Integer userNumber = Integer.parseInt(userGuessField.getText().toString());
            if (userNumber != null) {
                if (userNumber < number) {
                    Toast.makeText(getApplicationContext(), "Higher", Toast.LENGTH_LONG).show();
                } else if (userNumber > number) {
                    Toast.makeText(getApplicationContext(), "Lower", Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(getApplicationContext(), "You got it!", Toast.LENGTH_LONG).show();
                    Random randomGenerator = new Random();
                    number = randomGenerator.nextInt(100)+1;
                }
            }
        } catch(NumberFormatException e) {
            Log.e("Exception", "Unacceptable number");
            Toast.makeText(getApplicationContext(), "Please enter a number.", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        Random randomGenerator = new Random();
        number = randomGenerator.nextInt(100)+1;
        Log.i("Info", String.valueOf(number));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
