package com.example.bluemelodia.sounddemo;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.media.MediaPlayer;
import android.widget.SeekBar;
import android.media.AudioManager;
import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends AppCompatActivity {
    public static MediaPlayer mPlayer;
    AudioManager audioManager; // work with volume/audio on our system

    public void play(View view) {
        mPlayer.start();
    }

    public void pause(View view) {
        mPlayer.pause();
    }

    public void restart(View view) {
        mPlayer.seekTo(0);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // we need a context (this), and create it with the file itself
        mPlayer = MediaPlayer.create(this, R.raw.piano);

        // let us work with the audio on our device
        audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);

        // establish max and current volume, use them to update seek bar to start with
        int maxVolume = audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC); // generic stream for playing sound/music in apps
        int curVolume = audioManager.getStreamVolume(AudioManager.STREAM_MUSIC); // get cur vol

        // set the max vol of system to be max val of seek bar, so they can't exceed max sound
        SeekBar volumeControl = (SeekBar) findViewById(R.id.vol);
        volumeControl.setMax(maxVolume); // set to max vol of our system
        volumeControl.setProgress(curVolume);

        // catch event where user clicks/drags/releases the seek bar
        // listener that listens for the event that seek bar has been changed
        volumeControl.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            // implement what happens when the seek bar starts or stops being changed
            // here we only care about when it's been completely changed

            // we add our own code to a method that already exists - need Override
            // to prevent error, must implement methods for all the possibilities
            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {}

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {}

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                // fromUser - did the user change the seekbar value?
                Log.i("SeekBar value:", String.valueOf(progress));
                audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, progress, 0);
            }
        });

        int endOfSong = mPlayer.getDuration();
        //int currentSong = mPlayer.getCurrentPosition();

        final SeekBar seekControl = (SeekBar) findViewById(R.id.seek);
        seekControl.setMax(endOfSong);
        // this timer runs every 2 seconds, with the first run immediate
        // and it will update the seekControl to reflect where the song currently is
        new Timer().scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                seekControl.setProgress(mPlayer.getCurrentPosition());
            }
        }, 0, 2000);

        //seekControl.setProgress(currentSong);

        seekControl.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                mPlayer.pause();
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                mPlayer.start();
            }

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                // fromUser - did the user change the seekbar value?
                Log.i("SeekBar value:", String.valueOf(progress));
                mPlayer.seekTo(progress);
                //audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, progress, 0);
            }
        });

        // to play it
        //mplayer.start();

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}