# Android-Practice
```
Activity - a page of your app, similar to ViewController in iOS
  Only customize the activity if you have several of them
Gradle is the build engine for Android Studio - it is a build tool Android uses to create the app from the files you create
AndroidManifest.xml stores core/basic information about your app
MainActivity is the code that will run our app, we will be editing this most of the time
res contains all the resources
activity_main.xml has info 
Gradle Scripts let us run scripts each time the app is built

Lecture 11: Text Formatting
dp - density independent pixel, way of working with the fact that Android phones have different pixel densities/screen sizes
  roughly 1/60th of an inch
layout: center in parent: parent is the RelativeLayout, the white part of the screen
On the screen we have about 400dp in width
padding: within the object itself (includes the background color), margin: outside the object itself
text size: uses sp, scale-independent pixels - scales the size of what you are setting according to the user's text size on their system
dp, sp are identical from our POV, but for users, if they specified different text size, sp will scale it up or down according 
to their preferences. Use dp for almost everything except for text, unless you want the text to be absolutely fixed

Lecture 19: Images
Do not try to change the extension of the images you are copying into res/drawable. Otherwise, the emulator will not work.
Large images can crash the app!
```
