package com.example.bluemelodia.animations;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity {
    // We want image to fade out when we click on it
    public void fade(View view) {
        ImageView squirtle = (ImageView) findViewById(R.id.squirtle);
        ImageView wartortle = (ImageView) findViewById(R.id.wartortle);
        // Allows the next turtle to be clickable
        wartortle.bringToFront();
        // tell compiler what we want to change, and how long we want for the change to happen (ms)
        // 0 automatically converted to a float, but keep the f in for convention
        squirtle.animate().alpha(0f).setDuration(2000);
        wartortle.animate().alpha(1f).setDuration(2000);
    }

    public void fade2(View view) {
        ImageView wartortle = (ImageView) findViewById(R.id.wartortle);
        ImageView blastoise = (ImageView) findViewById(R.id.blastoise);
        blastoise.bringToFront();
        // tell compiler what we want to change, and how long we want for the change to happen (ms)
        // 0 automatically converted to a float, but keep the f in for convention
        wartortle.animate().alpha(0f).setDuration(2000);
        blastoise.animate().alpha(1f).setDuration(2000);
    }

    public void fade3(View view) {
        ImageView blastoise = (ImageView) findViewById(R.id.blastoise);
        ImageView squirtle = (ImageView) findViewById(R.id.squirtle);
        squirtle.bringToFront();
        // tell compiler what we want to change, and how long we want for the change to happen (ms)
        // 0 automatically converted to a float, but keep the f in for convention
        blastoise.animate().alpha(0f).setDuration(2000);
        squirtle.animate().alpha(1f).setDuration(2000);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
